//
//  BarView.swift
//  Zchart
//
//  Created by Sierra 4 on 03/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class BarView: UIView {
    var dataToRepresent:[[String:Any]] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
    }
    
//    override func draw(_ rect: CGRect) {
//        let currentGraphicsContext = UIGraphicsGetCurrentContext()
//        let center = CGPoint(x: self.frame.size.width / 2.0,y: self.frame.size.height / 2.0)
//        let rectangleWidth:CGFloat = 100.0
//        let rectangleHeight:CGFloat = 100.0
//        
//        //4
//        currentGraphicsContext!.addRect(CGRect(x:center.x - (0.5 * rectangleWidth),y: center.y - (0.5 * rectangleHeight),width: rectangleWidth,height: rectangleHeight))
//        currentGraphicsContext!.setLineWidth(10)
//        currentGraphicsContext!.setStrokeColor(UIColor.gray.cgColor)
//        currentGraphicsContext!.strokePath()
//        
//        //5
//        currentGraphicsContext!.setFillColor(UIColor.green.cgColor)
//        currentGraphicsContext!.addRect(CGRect(x:center.x - (0.5 * rectangleWidth),y: center.y - (0.5 * rectangleHeight),width: rectangleWidth,height: rectangleHeight))
//        
//        currentGraphicsContext?.fillPath()
//    }
    
    func addDataToRepresent(value:Int , color:UIColor){
        if(value>0 && color.isKind(of: UIColor.self)){
            dataToRepresent.append(["value":value,"color":color])
        }
        else {
            print("\nHey...you are passing invalid data to the aks straight pie chart...please correct it.")
        }
    }
    
    func clearChart() {
        dataToRepresent.removeAll()
    }
    
    override func draw(_ rect: CGRect) {
        guard let currentGraphicsContext = UIGraphicsGetCurrentContext() else { return }
        var progressRect = rect
        var color1 = UIColor(red: 149.0/255.0, green: 159.0/255.0, blue: 169.0/255.0, alpha: 1.0)
        var color2 = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        let percentage : CGFloat = 0.1
        progressRect.size.width *= percentage
        progressRect.origin.x = 0.20
        currentGraphicsContext.addRect(progressRect)
        currentGraphicsContext.setFillColor(color2.cgColor)
        currentGraphicsContext.fill(progressRect)
        
    }
}
