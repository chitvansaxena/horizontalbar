
//
//  SuvPieChart.swift
//  HorizontalPieChart
//
//  Created by Sierra 4 on 29/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class SuvPieChart: UIView {

    var dataToRepresent:[[String:Any]] = []
    
    override init(frame: CGRect){
        super.init(frame: frame)
        startUpSettings()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        startUpSettings()
    }
    
    override func awakeFromNib() {
        startUpSettings()
    }
    
    func startUpSettings(){
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.white
    }
    
    func addDataToRepresent(value:CGFloat,color:UIColor){
        if(value>0 && color.isKind(of: UIColor.self)){
            dataToRepresent.append(["value":value,"color":color])
        }
        else {
            print("\nHey...you are passing invalid data to the aks straight pie chart...please correct it.")
        }
    }
    
    func clearChart() {
        dataToRepresent.removeAll()
        self.setNeedsDisplay()
    }
    
    func fillRect(rect:CGRect, color:CGColor, context:CGContext){
        context.addRect(rect)
        context.setFillColor(color)
        DispatchQueue.global().async {
            DispatchQueue.main.async(execute: {
                context.fill(rect)
                let bpath:UIBezierPath = UIBezierPath(rect: rect)
                bpath.stroke()
            })
        }
    }
    override func draw(_ rect: CGRect){
        guard let currentGraphicsContext = UIGraphicsGetCurrentContext() else {
            return 
        }
        var sumOfAllSegmentValues:CGFloat = 0
        
        dataToRepresent.forEach{ (element) in
            sumOfAllSegmentValues += CGFloat(element["value"] as! CGFloat)
        }
        
        var progressRect = rect
        var lastSegmentRect = CGRect.init(x: 0, y: 0, width: 0, height: 0)
        
        dataToRepresent.forEach{ [weak self] (element) in
            let currentSegmentValue = element["value"] as! CGFloat
            let color = element["color"] as! CGColor
            let percentage = currentSegmentValue/sumOfAllSegmentValues
            
            progressRect = rect
            progressRect.size.width *= percentage
            progressRect.origin.x += lastSegmentRect.origin.x + lastSegmentRect.size.width
            progressRect.origin.y = lastSegmentRect.origin.y
            
            if(progressRect.origin.x > 0) {
                progressRect.origin.x += 2
            }
            lastSegmentRect = progressRect
            self?.fillRect(rect: lastSegmentRect, color: color, context: currentGraphicsContext)
//            currentGraphicsContext.addRect(lastSegmentRect)
//            currentGraphicsContext.setFillColor(color)
//            DispatchQueue.global().async {
//                DispatchQueue.main.async(execute: {
//                    currentGraphicsContext.fill(lastSegmentRect)
//                })
//            }

            
        }

    }

}
