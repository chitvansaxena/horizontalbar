//
//  ViewController.swift
//  Zchart
//
//  Created by Sierra 4 on 31/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
var dataToRepresent:[[String:Any]] = []
    
    @IBOutlet weak var barView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        clearChart()
        addDataToRepresent(value: 10, color: UIColor.green)
        addDataToRepresent(value: 20, color: UIColor.brown)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addDataToRepresent(value:Int , color:UIColor){
        if(value>0 && color.isKind(of: UIColor.self)){
            dataToRepresent.append(["value":value,"color":color])
        }
        else {
            print("\nHey...you are passing invalid data to the aks straight pie chart...please correct it.")
        }
    }
    
    func clearChart() {
        dataToRepresent.removeAll()
    }
}
